package th.co.ais.iwrtc.controller;

import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import com.google.gson.Gson;

import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.jaxb.InstanceContext;
import th.co.ais.iwrtc.utils.ConfigureTools;
import th.co.ais.iwrtc.utils.InstanceHandler;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.ECDialogue;
import ec02.af.data.EquinoxProperties;
import ec02.af.data.EquinoxRawData;
import ec02.af.exception.ActionProcessException;
import ec02.af.exception.ComposeInstanceException;
import ec02.af.exception.ConstructRawDataException;
import ec02.af.exception.ExtractInstanceException;
import ec02.af.exception.ExtractRawDataException;
import ec02.interfaces.IEC02;

public class APPController extends AbstractAF implements IEC02{

	@Override
	public ECDialogue actionProcess(EquinoxProperties eqxProp,ArrayList<EquinoxRawData> rawData, Object instance)
			throws ActionProcessException {
		
		EC02Instance ec02Instance = (EC02Instance) instance;
		ec02Instance.setEqxProp(eqxProp);
		ec02Instance.setAbstractAF(this);
		
		String currentState = eqxProp.getState();
		
		if(eqxProp.getRet().equals(RetNumber.TIMEOUT) || rawData.size() ==0){
			rawData.clear();
			
			EquinoxRawData rawDataOut = new EquinoxRawData();
			rawDataOut.setRet(RetNumber.TIMEOUT);
			rawDataOut.setType(MessageType.RESPONSE);
			
			rawData.add(rawDataOut);
		
		}
		
		
		
		StateManager stateManager = new StateManager(currentState);
		
		String nextState = stateManager.doAction(this, ec02Instance, rawData);
		
		eqxProp.setState(nextState);
		eqxProp.setRet(ec02Instance.getRet());
		eqxProp.setSession(ec02Instance.getSession());
		eqxProp.setDiag("");
		eqxProp.setTimeout(ec02Instance.getTimeout());
		
			
		return new ECDialogue(eqxProp, ec02Instance);
	}

	@Override
	public boolean verifyAFConfiguration(String instance) {
		
		ConfigureTools.initConfigureTool(this.getUtils().getHmWarmConfig());
		
		try {
			InstanceContext.initUserAuthorizeRequestContext();
			InstanceContext.initUserAuthorizeAnswerContext();
			InstanceContext.initLocationInfoRequestContext();
			InstanceContext.initLocationInfoAnswerContext();
			
		} catch (JAXBException e) {
			System.out.println( "Error Here" );
			e.printStackTrace();
			return false;
		}
		
		return true;

	}

	@Override
	public String composeInstance(Object instance) throws ComposeInstanceException {
//		Gson gson = new Gson();
		EC02Instance ec02Instance = (EC02Instance) instance;
		APPInstance appInstance = ec02Instance.getAppInstance();

//		return gson.toJson(appInstance);
		return InstanceHandler.encode(appInstance);
	}

	@Override
	public ArrayList<EquinoxRawData> constructRawData(Object instance)
			throws ConstructRawDataException {
		
		EC02Instance ec02Instance = (EC02Instance) instance;
		
		return ec02Instance.getEquinoxRawDataList();
	}

	@Override
	public Object extractInstance(String instance) throws ExtractInstanceException {
		
		EC02Instance ec02Instance = new EC02Instance();
		APPInstance appInstance =null;
		
		appInstance = (instance!=null && !instance.isEmpty())? InstanceHandler.decode(instance)
				: new APPInstance();
		ec02Instance.setAppInstance(appInstance);
		
		return ec02Instance;
	}

	@Override
	public void extractRawData(Object instance, ArrayList<EquinoxRawData> rawData)
			throws ExtractRawDataException {
		// TODO Auto-generated method stub
		
	}

}
