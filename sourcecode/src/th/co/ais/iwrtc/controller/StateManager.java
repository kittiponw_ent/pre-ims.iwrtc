package th.co.ais.iwrtc.controller;

import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.state.IDLE;
import th.co.ais.iwrtc.state.INITIATE_COMMUNICATION;
import th.co.ais.iwrtc.state.INITIATE_INVITE;
import th.co.ais.iwrtc.state.W_200;
import th.co.ais.iwrtc.state.W_2ND_REGISTER;
import th.co.ais.iwrtc.state.W_401;
import th.co.ais.iwrtc.state.W_ANSWER;
import th.co.ais.iwrtc.state.W_LIA;
import th.co.ais.iwrtc.state.W_RINGING;
import th.co.ais.iwrtc.state.W_UAA;
import ec02.af.abstracts.AbstractAFStateManager;

public class StateManager extends AbstractAFStateManager {
	public StateManager(String currentState){
		
		if(States.IDLE.equals(currentState)){
			this.afState=new IDLE();
		}
		else if(States.W_200.equals(currentState)){
			this.afState= new W_200();
		}
		else if(States.W_2ND_REGISTER.equals(currentState)){
			this.afState = new W_2ND_REGISTER();
		}
		
		else if(States.W_401.equals(currentState)){
			this.afState = new W_401();
		}
	
		else if(States.W_UAA.equals(currentState)){
			this.afState=new W_UAA();
		}
		
		else if(States.INITIATE_INVITE.equals(currentState)){
			this.afState = new INITIATE_INVITE();
		}
		
		else if(States.W_LIA.equals(currentState)){
			this.afState = new W_LIA();
		}
		
		else if(States.W_RINGING.equals(currentState)){
			this.afState = new W_RINGING();
		}
		
		else if(States.W_ANSWER.equals(currentState)){
			this.afState = new W_ANSWER();
		}
		
		else if(States.INITIIATE_COMMUNICATION.equals(currentState)){
			this.afState = new INITIATE_COMMUNICATION();
		}
		
	}	
}