package th.co.ais.iwrtc.instances;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement(name ="Data")
@XmlAccessorType(XmlAccessType.FIELD)
public class LocationInfoAnswer {
	
	@XmlPath("Session-Id/@value")
	private String sessionId;
	
	@XmlPath("Vendor-Specific-Application-Id/@value")
	private String vendorSpecApplicationID;
	
	@XmlPath("Result-Code/@value")
	private String resultCode;
	
	@XmlPath("Experimental-Result/@value")
	private String experimentalResult;
	
	@XmlPath("Auth-Session-State/@value")
	private String authSessionState;
	
	@XmlPath("Origin-Host/@value")
	private String originHost;
	
	@XmlPath("Origin-Realm/@value")
	private String originRealm;
	
	@XmlPath("OC-Supported-Features/@value")
	private String ocSupportFeature;
	
	@XmlPath("OC-OLR/@value")
	private String ocOLR;
	
	@XmlPath("Supported-Features/@value")
	private String supportFeature;
	
	@XmlPath("Server-Name/@value")
	private String serverName;
	
	@XmlPath("Server-Capabilities/@value")
	private String serverCapability;
	
	@XmlPath("Wildcarded-Public-Identity/@value")
	private String wildcardPublicIdentity;
	
	@XmlPath("LIA-Flags/@value")
	private String liaFlag;
	
	@XmlPath("Proxy-Info/@value")
	private String proxyInfo;
	
	@XmlPath("Route-Record/@value")
	private String routeRecord;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getVendorSpecApplicationID() {
		return vendorSpecApplicationID;
	}

	public void setVendorSpecApplicationID(String vendorSpecApplicationID) {
		this.vendorSpecApplicationID = vendorSpecApplicationID;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getExperimentalResult() {
		return experimentalResult;
	}

	public void setExperimentalResult(String experimentalResult) {
		this.experimentalResult = experimentalResult;
	}

	public String getAuthSessionState() {
		return authSessionState;
	}

	public void setAuthSessionState(String authSessionState) {
		this.authSessionState = authSessionState;
	}

	public String getOriginHost() {
		return originHost;
	}

	public void setOriginHost(String originHost) {
		this.originHost = originHost;
	}

	public String getOriginRealm() {
		return originRealm;
	}

	public void setOriginRealm(String originRealm) {
		this.originRealm = originRealm;
	}

	public String getOcSupportFeature() {
		return ocSupportFeature;
	}

	public void setOcSupportFeature(String ocSupportFeature) {
		this.ocSupportFeature = ocSupportFeature;
	}

	public String getOcOLR() {
		return ocOLR;
	}

	public void setOcOLR(String ocOLR) {
		this.ocOLR = ocOLR;
	}

	public String getSupportFeature() {
		return supportFeature;
	}

	public void setSupportFeature(String supportFeature) {
		this.supportFeature = supportFeature;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getServerCapability() {
		return serverCapability;
	}

	public void setServerCapability(String serverCapability) {
		this.serverCapability = serverCapability;
	}

	public String getWildcardPublicIdentity() {
		return wildcardPublicIdentity;
	}

	public void setWildcardPublicIdentity(String wildcardPublicIdentity) {
		this.wildcardPublicIdentity = wildcardPublicIdentity;
	}

	public String getLiaFlag() {
		return liaFlag;
	}

	public void setLiaFlag(String liaFlag) {
		this.liaFlag = liaFlag;
	}

	public String getProxyInfo() {
		return proxyInfo;
	}

	public void setProxyInfo(String proxyInfo) {
		this.proxyInfo = proxyInfo;
	}

	public String getRouteRecord() {
		return routeRecord;
	}

	public void setRouteRecord(String routeRecord) {
		this.routeRecord = routeRecord;
	}
	
	
}
