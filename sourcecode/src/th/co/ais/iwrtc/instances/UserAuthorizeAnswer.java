package th.co.ais.iwrtc.instances;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement(name ="Data")
@XmlAccessorType(XmlAccessType.FIELD)

public class UserAuthorizeAnswer {

	@XmlPath("Session-Id/@value")
	private String sessionId;
	
	@XmlPath("Vendor-Specific-Application-Id/@value")
	private String vender_Spec_Application_ID;
	
	@XmlPath("Result-Code/@value")
	private String resultCode;
	
	@XmlPath("Experimental-Result/@value")
	private String experimental_Result;
	
	@XmlPath("Auth-Session-State/@value")
	private String auth_Session_State;
	
	@XmlPath("Origin-Host/@value")
	private String origin_Host;
	
	@XmlPath("Origin-Realm/@value")
	private String origin_Realms;
	
	@XmlPath("OC-Supported-Features/@value")
	private String oc_Supported_Features;
	
	@XmlPath("OC-OLR/@value")
	private String oc_OLR;
	
	@XmlPath("Supported-Features/@value")
	private String supported_Features;
	
	@XmlPath("Server-Name/@value")
	private String server_Name;
	
	@XmlPath("Server-Capabilities/@value")
	private String server_Capabilities;
	
	@XmlPath("Proxy-Info/@value")
	private String proxy_Info;
	
	@XmlPath("Route-Record/@value")
	private String route_Record;

	
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getVender_Spec_Application_ID() {
		return vender_Spec_Application_ID;
	}

	public void setVender_Spec_Application_ID(String vender_Spec_Application_ID) {
		this.vender_Spec_Application_ID = vender_Spec_Application_ID;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getExperimental_Result() {
		return experimental_Result;
	}

	public void setExperimental_Result(String experimental_Result) {
		this.experimental_Result = experimental_Result;
	}

	public String getAuth_Session_State() {
		return auth_Session_State;
	}

	public void setAuth_Session_State(String auth_Session_State) {
		this.auth_Session_State = auth_Session_State;
	}

	public String getOrigin_Host() {
		return origin_Host;
	}

	public void setOrigin_Host(String origin_Host) {
		this.origin_Host = origin_Host;
	}

	public String getOrigin_Realms() {
		return origin_Realms;
	}

	public void setOrigin_Realms(String origin_Realms) {
		this.origin_Realms = origin_Realms;
	}

	public String getOc_Supported_Features() {
		return oc_Supported_Features;
	}

	public void setOc_Supported_Features(String oc_Supported_Features) {
		this.oc_Supported_Features = oc_Supported_Features;
	}

	public String getOc_OLR() {
		return oc_OLR;
	}

	public void setOc_OLR(String oc_OLR) {
		this.oc_OLR = oc_OLR;
	}

	public String getSupported_Features() {
		return supported_Features;
	}

	public void setSupported_Features(String supported_Features) {
		this.supported_Features = supported_Features;
	}

	public String getServer_Name() {
		return server_Name;
	}

	public void setServer_Name(String server_Name) {
		this.server_Name = server_Name;
	}

	public String getServer_Capabilities() {
		return server_Capabilities;
	}

	public void setServer_Capabilities(String server_Capabilities) {
		this.server_Capabilities = server_Capabilities;
	}

	public String getProxy_Info() {
		return proxy_Info;
	}

	public void setProxy_Info(String proxy_Info) {
		this.proxy_Info = proxy_Info;
	}

	public String getRoute_Record() {
		return route_Record;
	}

	public void setRoute_Record(String route_Record) {
		this.route_Record = route_Record;
	}
		
}
