package th.co.ais.iwrtc.instances;

import org.apache.commons.lang.StringEscapeUtils;

public class SIPResponse extends SIPMessage{
	
	private String version;
	private String responseCode;
	private String reasonCode;
	
	public SIPResponse(String version,String responseCode,String reasonCode){
		this.version = version;
		this.responseCode = responseCode;
		this.reasonCode = reasonCode;
		}
	
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	
	public String toString() {
		
 		StringBuffer s = new StringBuffer();
		
		s.append( this.getVersion() + " " + this.getResponseCode() + " " + this.getReasonCode()+" \n"  );
		
		for ( String k : this.getHeader().keySet() ) {
			
			s.append( k + ":" + this.getHeader().get( k ).get( 0 ) + " \r\n" );
		}
		
		s.append( this.getContent() );
		return StringEscapeUtils.escapeHtml( s.toString() );
	}
	
	
	
}
