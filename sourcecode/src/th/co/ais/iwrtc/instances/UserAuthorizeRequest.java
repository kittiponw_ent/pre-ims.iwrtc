package th.co.ais.iwrtc.instances;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement(name ="UserAuthorizationRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserAuthorizeRequest {

	@XmlPath("Session-Id/@value")
	private String sessionID;
	
	@XmlPath("Vendor-Specific-Application-Id/@value")
	private String vendorSpec;
	
	@XmlPath("Auth-Session-State/@value")
	private String authSessionState;
	
	@XmlPath("Origin-Host/@value")
	private String originHost;
	
	@XmlPath("Origin-Realm/@value")
	private String originReal;
	
	@XmlPath("Destination-Realm/@value")
	private String destinationReal;
	
	@XmlPath("Destination-Host/@value")
	private String destonationHost;
	
	@XmlPath("User-Name/@value")
	private String userName;
	
	@XmlPath("OC-Supported-Features/@value")
	private String ocSupportFeature;
	
	@XmlPath("Supported-Features/@value")
	private String supportFeature;
	
	@XmlPath("Public-Identity/@value")
	private String publicIdentity;
	
	@XmlPath("Visited-Network-Identifier/@value")
	private String visitNetworkIdentify;
	
	@XmlPath("User-Authorization-Type/@value")
	private String userAuthorizationType;
	
	@XmlPath("UAR-Flags/@value")
	private String uarFlag;
	
	@XmlPath("Proxy-Info/@value")
	private String proxyInfo;
	
	@XmlPath("Route-Record/@value")
	private String routeReccord;
	
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getVendorSpec() {
		return vendorSpec;
	}
	public void setVendorSpec(String vendorSpec) {
		this.vendorSpec = vendorSpec;
	}
	public String getAuthSessionState() {
		return authSessionState;
	}
	public void setAuthSessionState(String authSessionState) {
		this.authSessionState = authSessionState;
	}
	public String getOriginHost() {
		return originHost;
	}
	public void setOriginHost(String originHost) {
		this.originHost = originHost;
	}
	public String getOriginReal() {
		return originReal;
	}
	public void setOriginReal(String originReal) {
		this.originReal = originReal;
	}
	public String getDestinationReal() {
		return destinationReal;
	}
	public void setDestinationReal(String destinationReal) {
		this.destinationReal = destinationReal;
	}
	public String getDestonationHost() {
		return destonationHost;
	}
	public void setDestonationHost(String destonationHost) {
		this.destonationHost = destonationHost;
	}
	public String getUserNaame() {
		return userName;
	}
	public void setUserNaame(String userNaame) {
		this.userName = userNaame;
	}
	public String getOcSupportFeature() {
		return ocSupportFeature;
	}
	public void setOcSupportFeature(String ocSupportFeature) {
		this.ocSupportFeature = ocSupportFeature;
	}
	public String getSupportFeature() {
		return supportFeature;
	}
	public void setSupportFeature(String supportFeature) {
		this.supportFeature = supportFeature;
	}
	public String getPublicIdentity() {
		return publicIdentity;
	}
	public void setPublicIdentity(String publicIdentity) {
		this.publicIdentity = publicIdentity;
	}
	public String getVisitNetworkIdentify() {
		return visitNetworkIdentify;
	}
	public void setVisitNetworkIdentify(String visitNetworkIdentify) {
		this.visitNetworkIdentify = visitNetworkIdentify;
	}
	public String getUserAuthorizationType() {
		return userAuthorizationType;
	}
	public void setUserAuthorizationType(String userAuthorizationType) {
		this.userAuthorizationType = userAuthorizationType;
	}
	public String getUarFlag() {
		return uarFlag;
	}
	public void setUarFlag(String uarFlag) {
		this.uarFlag = uarFlag;
	}
	public String getProxyInfo() {
		return proxyInfo;
	}
	public void setProxyInfo(String proxyInfo) {
		this.proxyInfo = proxyInfo;
	}
	public String getRouteReccord() {
		return routeReccord;
	}
	public void setRouteReccord(String routeReccord) {
		this.routeReccord = routeReccord;
	}
	
}
