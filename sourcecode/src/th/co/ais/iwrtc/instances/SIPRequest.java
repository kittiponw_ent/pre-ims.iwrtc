package th.co.ais.iwrtc.instances;

import org.apache.commons.lang.StringEscapeUtils;

public class SIPRequest extends SIPMessage {

	private String method;
	private String uri;
	private String version;
	
	
	public SIPRequest (String method , String uri , String version){
		this.method = method;
		this.uri = uri;
		this.version = version;
	}
	
	
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String toString() {
		
		StringBuffer s = new StringBuffer();
		
		s.append( String.format( "%s %s %s\n", this.getMethod(), this.getUri(), this.getVersion() ) );
		
		for ( String k : this.getHeader().keySet() ) {
			s.append( k + ":" + this.getHeader().get( k ).get( 0 ) + " \r\n" );
			
		}

		s.append( this.getContent() );
		return StringEscapeUtils.escapeHtml( s.toString() );
	}
	
	
}
