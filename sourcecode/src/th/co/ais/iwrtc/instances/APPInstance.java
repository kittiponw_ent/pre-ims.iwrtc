package th.co.ais.iwrtc.instances;

import java.util.ArrayList;

import ec02.af.data.EquinoxRawData;

public class APPInstance {

	public String invoke;
	public String session;
	public ArrayList<EquinoxRawData> rawDataList;
	public SIPMessage sipMsgInstance;
	public SIPRequest sipRequstInstance;
	public SIPResponse sipResponseInstance;
	public UserAuthorizeAnswer uaaInstance;
	public LocationInfoAnswer liaInstance;
	public String orig;
	public long startTime;
	public String state;
	public String initOrig;
	public String initInvoke;
	
	
	public String getInitOrig() {
		return initOrig;
	}
	public void setInitOrig(String initOrig) {
		this.initOrig = initOrig;
	}
	public String getInitInvoke() {
		return initInvoke;
	}
	public void setInitInvoke(String initInvoke) {
		this.initInvoke = initInvoke;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public SIPRequest getSipRequstInstance() {
		return sipRequstInstance;
	}
	public void setSipRequstInstance(SIPRequest sipRequstInstance) {
		this.sipRequstInstance = sipRequstInstance;
	}
	public SIPResponse getSipResponseInstance() {
		return sipResponseInstance;
	}
	public void setSipResponseInstance(SIPResponse sipResponseInstance) {
		this.sipResponseInstance = sipResponseInstance;
	}
	
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public SIPMessage getSipMsgInstance() {
		return sipMsgInstance;
	}
	public void setSipMsgInstance(SIPMessage sipMsgInstance) {
		this.sipMsgInstance = sipMsgInstance;
	}
	public String getOrig() {
		return orig;
	}
	public void setOrig(String orig) {
		this.orig = orig;
	}

	public ArrayList<EquinoxRawData> getRawDataList() {
		return rawDataList;
	}
	public void setRawDataList(ArrayList<EquinoxRawData> rawData) {
		this.rawDataList = rawData;
	}
	public String getInvoke() {
		return invoke;
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}

	public UserAuthorizeAnswer getUaaInstance() {
		return uaaInstance;
	}
	public void setUaaInstance(UserAuthorizeAnswer uaaInstance) {
		this.uaaInstance = uaaInstance;
	}
	public LocationInfoAnswer getLiaInstance() {
		return liaInstance;
	}
	public void setLiaInstance(LocationInfoAnswer liaInstance) {
		this.liaInstance = liaInstance;
	}
	
}
