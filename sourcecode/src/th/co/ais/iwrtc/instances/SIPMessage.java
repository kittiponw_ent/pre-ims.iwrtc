package th.co.ais.iwrtc.instances;

import java.util.ArrayList;
import java.util.HashMap;



public class SIPMessage {
 
	
	private StringBuilder content;
	private HashMap<String, ArrayList<String>> header;
	
	
	public SIPMessage(){
		setHeader(new HashMap<String, ArrayList<String>>());
		setContent(new StringBuilder());
		}


	public StringBuilder getContent() {
		return content;
	}


	public void setContent(StringBuilder content) {
		this.content = content;
	}


	public HashMap<String, ArrayList<String>> getHeader() {
		return header;
	}


	public void setHeader(HashMap<String, ArrayList<String>> header) {
		this.header = header;
	}
	
}
