package th.co.ais.iwrtc.instances;

import java.util.ArrayList;

import th.co.ais.iwrtc.enums.Alarm;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxProperties;
import ec02.af.data.EquinoxRawData;

public class EC02Instance {

	private APPInstance appInstance;
	private String ret;
	private String timeout;
	private AbstractAF abstractAF;
	private EquinoxProperties eqxProp;
	private String session;
	private ArrayList<EquinoxRawData> equinoxRawDataList;
	private String diag;

	
	public String getDiag() {
		return diag;
	}
	public void setDiag(String diag) {
		this.diag = diag;
	}
	public ArrayList<EquinoxRawData> getEquinoxRawDataList() {
		return equinoxRawDataList;
	}
	public void setEquinoxRawDataList(ArrayList<EquinoxRawData> equinoxRawDataList) {
		this.equinoxRawDataList = equinoxRawDataList;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public AbstractAF getAbstractAF() {
		return abstractAF;
	}
	public void setAbstractAF(AbstractAF abstractAF) {
		this.abstractAF = abstractAF;
	}
	public EquinoxProperties getEqxProp() {
		return eqxProp;
	}
	public void setEqxProp(EquinoxProperties eqxProp) {
		this.eqxProp = eqxProp;
	}
	public APPInstance getAppInstance() {
		return appInstance;
	}
	public void setAppInstance(APPInstance appInstance) {
		this.appInstance = appInstance;
	}
	public String getRet() {
		return ret;
	}
	public void setRet(String ret) {
		this.ret = ret;
	}
	public String getTimeout() {
		return timeout;
	}
	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}
	
	public void raisAlarm(Alarm alarm , String[] param){
		
		this.abstractAF.getUtils().raiseAlarm(alarm.getAlarmName(), param, alarm.getSeverity(), alarm.getCategory(), alarm.getType());
		
	}
	
	public void incrementStat(String statName){
		
		this.abstractAF.getUtils().incrementStats(statName);
	}
	
	
}
