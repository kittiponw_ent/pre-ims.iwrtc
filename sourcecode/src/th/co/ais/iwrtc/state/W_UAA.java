package th.co.ais.iwrtc.state;


import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.enums.SIPResponseCode;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPRequest;
import th.co.ais.iwrtc.instances.SIPResponse;
import th.co.ais.iwrtc.instances.UserAuthorizeAnswer;
import th.co.ais.iwrtc.instances.UserAuthorizeRequest;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.SIPMsgHeader;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.jaxb.InstanceContext;
import th.co.ais.iwrtc.jaxb.JAXBHandle;
import th.co.ais.iwrtc.utils.InstanceHandler;
import th.co.ais.iwrtc.utils.InvokeGenerator;
import th.co.ais.iwrtc.validator.UserAuthorizeAnswerValidate;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_UAA implements IAFState {
	
	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private String currentState;
	private String nextState;
	private UserAuthorizeAnswer userAuthorizeAnswer;
	private SIPRequest sipRequestInstance;
	private long startTime;
	private long transactionTime;
	
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
		
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		this.currentState = af.getEquinoxProperties().getState();
		this.listOutput = new ArrayList<EquinoxRawData>();
		this.currentState = af.getEquinoxProperties().getState();
		HashMap<String, String> attr=null;
		
		EquinoxRawData rawDataInput = rawData.get(0);

		sipRequestInstance =  this.appInstance.getSipRequstInstance();
		
		if(RetNumber.NORMAL.equals(rawDataInput.getRet())){
		
		try {
			
			this.userAuthorizeAnswer = (UserAuthorizeAnswer) JAXBHandle.createInstance(InstanceContext.getUserAuthorizeAnswerContext(),"<Data>"+rawDataInput.getRawDataMessage()+"</Data>", UserAuthorizeAnswer.class);
//			UserAuthorizeAnswerValidate.verifyMessage(this.userAuthorizeAnswer);
			
			if(! this.userAuthorizeAnswer.getResultCode().equals("2000")){
				
//				HashMap<String, String> sipHeader = new HashMap<String, String>();
//				sipHeader.put(SIPMsgHeader.VIA , this.appInstance.getSipRequstInstance().getHeader().get("Via").get(0));
//				sip
								
				String version = "SIP/2.0";
				String responseCode = "500";
				String reasonCode = "Server Internal Error";
				
				SIPResponse sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.appInstance.getSipRequstInstance().getHeader());
				
				attr = new HashMap<String, String>();
				attr.put("name", "SOCKET");
				attr.put("to", this.appInstance.getInitOrig());
				attr.put("ctype", "udp");
				attr.put("type", MessageType.RESPONSE );
				attr.put("invoke", this.appInstance.getInitInvoke());
				attr.put("val", sipResponse.toString());
				
				EquinoxRawData rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.TERMINATE);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
			}//if check result code is not 2000
			
			else{
			
			attr = new HashMap<String, String>();
			attr.put("name", "SOCKET");
			attr.put("to", "iWRTC.ES01.SWRTC.0");
			attr.put("ctype", "udp");
			attr.put("type", MessageType.REQUEST );
			attr.put("invoke", InvokeGenerator.getInvokeWithPrefix(this.currentState+"-"+System.currentTimeMillis()));
			attr.put("val", sipRequestInstance.toString());
			
			EquinoxRawData rawDataOut = new EquinoxRawData();
			rawDataOut.setRawDataAttributes(attr);
			rawDataOut.setRet(RetNumber.NORMAL);
			
			this.listOutput.add(rawDataOut);
			this.appInstance.setUaaInstance(this.userAuthorizeAnswer);
			this.nextState = States.W_401;
			
			}//else check result code is 2000
			
		} catch (Exception e) {
			
			e.printStackTrace();
				
		}//catch
		
		}//close if check equinox ret 
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawDataInput.getRet());
		
			SIPResponse sipResponse = null;
			EquinoxRawData rawDataOut = null;
			String version="SIP/2.0",responseCode="",reasonCode="";
			
			attr = new HashMap<String, String>();
			attr.put("name", "SOCKET");
			attr.put("to", this.appInstance.getInitOrig());
			attr.put("ctype", "udp");
			attr.put("type", MessageType.RESPONSE );
			attr.put("invoke", this.appInstance.getInitInvoke());
			attr.put("ret", RetNumber.TERMINATE);
			
			switch(e){
			
			case ERROR:
				responseCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getCode();
				reasonCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				break;
			
			case REJECT:
				responseCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getCode();
				reasonCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				
				break;
			
			case ABORT:
				responseCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getCode();
				reasonCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				
				break;
			
			case TIMEOUT:
				responseCode = SIPResponseCode.SERVER_TIME_OUT.getCode();
				reasonCode = SIPResponseCode.SERVER_TIME_OUT.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				break;
			
			default:
				
				break;
				
			}
			
	
		}//else check equinox ret
		
		
		this.transactionTime = System.currentTimeMillis() - this.startTime;
		
		this.appInstance.setInvoke(rawDataInput.getInvoke());
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setEqxProp(af.getEquinoxProperties());
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		this.ec02Instance.setRet(rawDataInput.getRet());
		this.ec02Instance.setTimeout("10");
		
		return this.nextState;
	}//method

}//class
