package th.co.ais.iwrtc.state;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.enums.SIPMethodRequest;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPRequest;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.utils.InvokeGenerator;
import th.co.ais.iwrtc.utils.SIPParser;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_2ND_REGISTER implements IAFState{

	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private String nextState;
	private String currentState;
	private SIPRequest sipRequestInstance;
	private SIPMessage sipMessageInstance;
	private long startTime;
	private long transactionTime;
	@Override
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
	
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		this.listOutput = new ArrayList<EquinoxRawData>();
		this.currentState = af.getEquinoxProperties().getState();
		HashMap<String, String> attr = null;
		String messageInput =null;
		EquinoxRawData rawdataInput = rawData.get(0);
		
		if(RetNumber.NORMAL.equals(rawdataInput.getRet())){
		messageInput = rawdataInput.getRawDataCDATAAttributes("val");
		
		try {
			
			this.sipMessageInstance = SIPParser.getMessage(messageInput);
			
			this.sipRequestInstance = (SIPRequest) this.sipMessageInstance;
			
			if(sipRequestInstance.getMethod().equals(SIPMethodRequest.REGISTER.getMethod())){
				
				attr = new HashMap<String, String>();
				attr.put("name", "SOCKET");
				attr.put("to", "iWRTC.ES01.SWRTC.0");
				attr.put("type", MessageType.REQUEST);
				attr.put("ctype", "udp");
				attr.put("invoke", InvokeGenerator.getInvokeWithPrefix(this.currentState+"-"+System.currentTimeMillis()));
				attr.put("val", this.sipRequestInstance.toString());
				
				EquinoxRawData rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.NORMAL);
				
				this.listOutput.add(rawDataOut);
				this.nextState = States.W_200;
				
			}//if check method register
			
			else{
				
			
				
				
				
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		}//if check ret 0
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawdataInput.getRet());
			
			switch (e) {
			case ERROR:
				
				break;

			
			case REJECT:
				
				
				break;
				
				
			case ABORT:
				
				break;
				
			case TIMEOUT:
				
				
				break;
			
			
			default:
				break;
			}
			
			
			
			
		}//else check ret 1,2,3,4
		
		this.transactionTime = System.currentTimeMillis() - this.startTime;
		
		this.appInstance.setOrig(rawdataInput.getOrig());
		this.appInstance.setInvoke(rawdataInput.getInvoke());
		this.appInstance.setSipRequstInstance(this.sipRequestInstance);
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setTimeout("10");
		
		return this.nextState;
	}

}
