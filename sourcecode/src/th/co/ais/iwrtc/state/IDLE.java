package th.co.ais.iwrtc.state;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.SIPMethodRequest;
import th.co.ais.iwrtc.exceptions.SIPParserException;
import th.co.ais.iwrtc.exceptions.ValidatateException;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.LocationInfoRequest;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPRequest;
import th.co.ais.iwrtc.instances.UserAuthorizeRequest;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.jaxb.InstanceContext;
import th.co.ais.iwrtc.jaxb.JAXBHandle;
import th.co.ais.iwrtc.utils.InvokeGenerator;
import th.co.ais.iwrtc.utils.SIPParser;
import th.co.ais.iwrtc.validator.SIPRequestValidate;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;
import ec02.utils.AppLog;

public class IDLE implements IAFState {

	private EC02Instance ec02Instance;
	private APPInstance appInstance;
	private ArrayList<EquinoxRawData> listoutput;
	private String nextState;
	private String currentState;
	private long startTime;
	private long transactionTime;
	
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		this.listoutput = new ArrayList<EquinoxRawData>();
		this.currentState = af.getEquinoxProperties().getState();
		HashMap<String, String> attr =null;
		SIPRequest sipRequest = null;
		EquinoxRawData rawDataInput = rawData.get(0);
		
		try {
			sipRequest = (SIPRequest) SIPParser.getMessage(rawDataInput.getRawDataCDATAAttributes("val"));
//			RequestValidate.verifyMessage(sipRequest);
			
			if(sipRequest.getMethod().equals(SIPMethodRequest.REGISTER.getMethod())){

				UserAuthorizeRequest uar = new UserAuthorizeRequest();
				uar.setSessionID("ec2-54-197-180-128.compute-1.amazonaws.com;1396640817;5");
				uar.setDestinationReal("toro.ais.co.th");
				uar.setOriginHost("GatewayService-5-12-0.3SUK4NS.awn.com");
				uar.setOriginReal("awn.com");
				uar.setDestonationHost("ocf");
//				uar.setAuthSessionState("sfds@sfsf.com");
				
	
				attr = new HashMap<String, String>();
				
				attr.put( "name", "DIAMETER" );
				attr.put( "ctype", "Credit-Control" );
				attr.put("type", MessageType.REQUEST);
				attr.put("to", "iWRTC.ES06.MAAA.0");
				attr.put("invoke", InvokeGenerator.getInvokeWithPrefix(this.currentState+"-"+System.currentTimeMillis()));
				
						
				EquinoxRawData rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRawMessage(JAXBHandle.composeMessageWithoutRoot(InstanceContext.getUserAuthorizeRequestContext(),uar));
				rawDataOut.setRet(RetNumber.NORMAL);
				
				this.appInstance.setSipRequstInstance(sipRequest);
				this.listoutput.add(rawDataOut);	
				
				this.nextState = States.W_UAA;
				
			}
			
			else if(sipRequest.getMethod().equals(SIPMethodRequest.INVITE.getMethod())){
				
				LocationInfoRequest lir = new LocationInfoRequest();
				lir.setSessionID("0:2082942334:PDP1.b11.CWD2N.ais.com;313;0200384357");
				lir.setDestinationReal("toro.ais.co.th");
				lir.setOriginHost("GatewayService-5-12-0.3SUK4NS.awn.com");
				lir.setOriginReal("awn.com");
				lir.setDestonationHost("ocf");
				
				
				attr = new HashMap<String, String>();
				
				attr.put( "name", "DIAMETER" );
				attr.put( "ctype", "Credit-Control" );
				attr.put("type", MessageType.REQUEST);
				attr.put("to", "iWRTC.ES06.MAAA.0");
				attr.put("invoke", InvokeGenerator.getInvokeWithPrefix(this.currentState+"-"+System.currentTimeMillis()));
				
				
				EquinoxRawData rawDataOut = new EquinoxRawData();
				rawDataOut.setRawMessage(JAXBHandle.composeMessageWithoutRoot(InstanceContext.getLocationInfoRequestContext(), lir));
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.NORMAL);
			
				this.appInstance.setSipRequstInstance(sipRequest);
				this.listoutput.add(rawDataOut);
				this.nextState = States.W_LIA;
					
			}
			
			
		} catch ( SIPParserException e) {
			e.printStackTrace();
			this.nextState = States.IDLE;
		}	
		
//		catch (ValidatateException validateException) {
//			validateException.printStackTrace();
//			this.nextState = States.IDLE;
//		}
		
		this.transactionTime = System.currentTimeMillis() - this.startTime;
		
		this.appInstance.setInvoke(rawDataInput.getInvoke());
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.appInstance.setStartTime(this.startTime);
		this.appInstance.setInitOrig(rawDataInput.getOrig());
		this.appInstance.setInitInvoke(rawDataInput.getInvoke());
		this.ec02Instance.setEquinoxRawDataList(this.listoutput);
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setEqxProp(af.getEquinoxProperties());
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setTimeout("10");
		
		return this.nextState;
	}
	
}
