package th.co.ais.iwrtc.state;


import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.enums.SIPResponseCode;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPResponse;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.utils.SIPParser;
import th.co.ais.iwrtc.validator.SIPResponseValidate;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_401 implements IAFState {

	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private String nextState;
	private String currentState;
	private SIPMessage sipMsgInstance;
	private SIPResponse sipResponseInstance;
	private long startTime;
	private long transactionTime;
	
	
	@Override
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
		
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		this.listOutput = new ArrayList<EquinoxRawData>();
		this.currentState = af.getEquinoxProperties().getState();
		HashMap<String, String> attr;
		
		EquinoxRawData rawDataInput = rawData.get(0);
		
		
		if(RetNumber.NORMAL.equals(rawDataInput.getRet())){
			
			try {
				
				sipMsgInstance = SIPParser.getMessage(rawDataInput.getRawDataCDATAAttributes("val"));
//				ResponseValidate.verifyMessage(sipMsgInstance);
				
				sipResponseInstance = (SIPResponse) sipMsgInstance;
				
				SIPResponseCode sipResponseCode = SIPResponseCode.getSipResponseCodeFrom(sipResponseInstance.getResponseCode());				
				
				switch(sipResponseCode){
				
					
				case UNAUTHORIZED:
					
					attr = new HashMap<String, String>();
					
					attr.put("name", "SOCKET");
					attr.put("to", this.appInstance.getInitOrig());
					attr.put("type", MessageType.RESPONSE);
					attr.put("ctype", "udp");
					attr.put("invoke", this.appInstance.getInitInvoke());
					attr.put("val", sipResponseInstance.toString() );
					
					EquinoxRawData rawDataOut = new EquinoxRawData();
					rawDataOut.setRawDataAttributes(attr);
					rawDataOut.setRet(RetNumber.NORMAL);
					
					this.listOutput.add(rawDataOut);
					
					this.nextState = States.W_2ND_REGISTER;
					
					break;
					
				default:
					String version = "SIP/2.0",
						   responseCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getCode(),
						   reasonCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode();
					
					SIPResponse sipResponse = new SIPResponse(version, responseCode, reasonCode);
					sipResponse.setHeader(sipResponseInstance.getHeader());
					
					attr = new HashMap<String, String>();
					
					attr.put("name", "SOCKET");
					attr.put("ctype", "udp");
					attr.put("type", MessageType.RESPONSE);
					attr.put("invoke", this.appInstance.getInitInvoke());
					attr.put("to", this.appInstance.getInitOrig());
					attr.put("val", sipResponse.toString());
					
					rawDataOut = new EquinoxRawData();
					rawDataOut.setRawDataAttributes(attr);
					rawDataOut.setRet(RetNumber.TERMINATE);
					
					this.listOutput.add(rawDataOut);
					
					this.nextState = States.IDLE;
	
					break;
				
				}
				
					
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}//equinox ret 0
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawDataInput.getRet());
			
			switch(e){
			
			case ERROR:
				
				
				break;
			
			case REJECT:
				
				break;
				
			case ABORT:
				
				
				break;
				
			
			case TIMEOUT:
				
				
				break;
				
				
			default:
				
				
				break;
			
			
			}//switch check ret
			
		
		}//equinox ret 1,2,3,4
		
		
		this.transactionTime = System.currentTimeMillis() - this.startTime ;
		
		this.appInstance.setInvoke(rawDataInput.getInvoke());
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.appInstance.setSession(af.getEquinoxProperties().getSession());
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setTimeout("10");
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setEqxProp(af.getEquinoxProperties());
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		this.ec02Instance.setAbstractAF(af);
		
		return this.nextState;
	}

}
