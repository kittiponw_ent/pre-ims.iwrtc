package th.co.ais.iwrtc.state;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.enums.SIPResponseCode;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.LocationInfoAnswer;
import th.co.ais.iwrtc.instances.LocationInfoRequest;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPRequest;
import th.co.ais.iwrtc.instances.SIPResponse;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.jaxb.InstanceContext;
import th.co.ais.iwrtc.jaxb.JAXBHandle;
import th.co.ais.iwrtc.utils.InvokeGenerator;
import th.co.ais.iwrtc.validator.LocationInfoAnswerValidate;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_LIA implements IAFState {

	private String nextState;
	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private String currentState;
	private SIPRequest sipRequestInstance;
	private LocationInfoAnswer liaInstance;
	private long startTime;
	private long transactionTime;
	
	@Override
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
		
		this.startTime = System.currentTimeMillis();
		this.currentState = af.getEquinoxProperties().getState();
		this.ec02Instance = (EC02Instance) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		this.listOutput = new ArrayList<EquinoxRawData>();
		
		HashMap< String , String > attr = null;
		
		EquinoxRawData rawDataInput = rawData.get(0);
		
		if(RetNumber.NORMAL.equals(rawDataInput.getRet())){
		
		try {
			this.sipRequestInstance = this.appInstance.getSipRequstInstance();
			this.liaInstance = (LocationInfoAnswer) JAXBHandle.createInstance(InstanceContext.getLocationInfoAnswerContext(),"<Data>"+ rawDataInput.getRawDataMessage()+"</Data>", LocationInfoAnswer.class);
//			LocationInfoAnswerValidate.verifyMessage(liaInstance);
			
			if(!this.liaInstance.getResultCode().equals("2000")){
				
				String version = "SIP/2.0";
				String responseCode = "500";
				String reasonCode = "Server Internal Error";
				
				SIPResponse sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				
				attr = new HashMap<String, String>();
				attr.put("name", "SOCKET");
				attr.put("to", this.appInstance.getInitOrig());
				attr.put("ctype", "udp");
				attr.put("type", MessageType.RESPONSE );
				attr.put("invoke", this.appInstance.getInitInvoke());
				attr.put("val", sipResponse.toString());
				
				EquinoxRawData rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.TERMINATE);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
			}//if result-code is not 2000
			
			else{
				
				attr = new HashMap<String, String>();
				attr.put("name", "SOCKET");
				attr.put("to", "iWRTC.ES01.SWRTC.0");
				attr.put("ctype", "udp");
				attr.put("type", MessageType.REQUEST );
				attr.put("invoke", InvokeGenerator.getInvokeWithPrefix(this.currentState+"-"+System.currentTimeMillis()));
				attr.put("val", this.sipRequestInstance.toString());
				
				EquinoxRawData rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.NORMAL);
				
				this.appInstance.setLiaInstance(this.liaInstance);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.INITIATE_INVITE;
				
			}//else result-code is 2000
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}//catch
		
		}//if ret is 0
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawDataInput.getRet());
			
			SIPResponse sipResponse = null;
			EquinoxRawData rawDataOut = null;
			String version="SIP/2.0",responseCode="",reasonCode="";
			
			attr = new HashMap<String, String>();
			attr.put("name", "SOCKET");
			attr.put("to", this.appInstance.getInitOrig());
			attr.put("ctype", "udp");
			attr.put("type", MessageType.RESPONSE );
			attr.put("invoke", this.appInstance.getInitInvoke());
			
			switch(e){
			
			case ERROR:
				responseCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getCode();
				reasonCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.TERMINATE);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				break;
			
			case REJECT:
				responseCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getCode();
				reasonCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.TERMINATE);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				
				break;
			
			case ABORT:
				responseCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getCode();
				reasonCode = SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.TERMINATE);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				
				break;
			
			case TIMEOUT:
				responseCode = SIPResponseCode.SERVER_TIME_OUT.getCode();
				reasonCode = SIPResponseCode.SERVER_TIME_OUT.getReasonCode();
				
				sipResponse = new SIPResponse(version, responseCode, reasonCode);
				sipResponse.setHeader(this.sipRequestInstance.getHeader());
				attr.put("val", sipResponse.toString());
				
				rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.TERMINATE);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.IDLE;
				
				break;
			
			default:
				
				break;
				
			}//switch check ret 1,2,3,4
			
		}//else ret is 1,2,3,4

		this.transactionTime = System.currentTimeMillis() - this.startTime;
		
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.appInstance.setInvoke(rawDataInput.getInvoke());
		this.appInstance.setLiaInstance(liaInstance);
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setTimeout("10");
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		
		return this.nextState;
	}

}
