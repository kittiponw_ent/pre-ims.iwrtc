package th.co.ais.iwrtc.state;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.enums.SIPMethodRequest;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPRequest;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.utils.InvokeGenerator;
import th.co.ais.iwrtc.utils.SIPParser;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class INITIATE_COMMUNICATION implements IAFState{

	private String nextState;
	private String currentState;
	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private SIPMessage sipMsgInstance;
	private SIPRequest  sipRequestInstance;
	private long startTime;
	private long transactionTime;
	
	@Override
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
	
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.listOutput = new ArrayList<EquinoxRawData>();
		this.appInstance = this.ec02Instance.getAppInstance();
		this.currentState = af.getEquinoxProperties().getState();
		HashMap<String, String> attr =null;
			
		EquinoxRawData rawDataInput = rawData.get(0);
		EquinoxRawData rawDataOut = null;
		
		if(RetNumber.NORMAL.equals(rawDataInput.getRet())){
		
			try {
				
				this.sipMsgInstance = SIPParser.getMessage(rawDataInput.getRawDataCDATAAttributes("val"));
				
				this.sipRequestInstance = (SIPRequest) this.sipMsgInstance;
				
				if(this.sipRequestInstance.getMethod().equals(SIPMethodRequest.ACK.getMethod())){
					
					attr = new HashMap<String, String>();
					
					attr.put("name", "SOCKET");
					attr.put("to", this.appInstance.getOrig());
					attr.put("invoke", this.appInstance.getInvoke());
					attr.put("type", MessageType.RESPONSE);
					attr.put("ctype", "udp");
					attr.put("val", this.sipRequestInstance.toString());
					
					rawDataOut = new EquinoxRawData();
					
					rawDataOut.setRawDataAttributes(attr);
					rawDataOut.setRet(RetNumber.TERMINATE);
					
					this.listOutput.add(rawDataOut);
					
					this.nextState = States.IDLE;
					
				}
				
				else{
					
					
					
					
					
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawDataInput.getRet());
			
			switch (e) {
			case ERROR:
				
				break;

			case REJECT:
				
				
				break;
				
			case ABORT:
				
				
				break;
				
				
			case TIMEOUT:
				
				
				break;
				
				
			default:
				break;
			}
			
			
		}
		
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.appInstance.setInvoke(rawDataInput.getOrig());
		this.appInstance.setSipRequstInstance(this.sipRequestInstance);
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setAppInstance(appInstance);
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		this.ec02Instance.setTimeout("10");
		this.ec02Instance.setEqxProp(af.getEquinoxProperties());
		
	
		return this.nextState;
	}

	
}
