package th.co.ais.iwrtc.state;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.enums.SIPResponseCode;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPResponse;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.utils.InvokeGenerator;
import th.co.ais.iwrtc.utils.SIPParser;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_RINGING implements IAFState {

	private String nextState;
	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private long startTime;
	private long transactionTime;
	private SIPMessage sipMsgInstance;
	private SIPResponse sipResponseInstance;
	private String currentState;
	
	@Override
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
		this.currentState = af.getEquinoxProperties().getState();
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		this.listOutput = new ArrayList<EquinoxRawData>();
		HashMap<String, String> attr =null;
		
		EquinoxRawData rawDataOut =null;
		EquinoxRawData rawDataInput = rawData.get(0);
		
		
		if(rawDataInput.getRet().equals(RetNumber.NORMAL)){
			
			try {
				
				this.sipMsgInstance = SIPParser.getMessage(rawDataInput.getRawDataCDATAAttributes("val"));
				this.sipResponseInstance  = (SIPResponse) this.sipMsgInstance;
				
				SIPResponseCode sipResponseCode = SIPResponseCode.getSipResponseCodeFrom(this.sipResponseInstance.getResponseCode());
				
				switch (sipResponseCode) {
				case RINGING:
					
					attr = new HashMap<String, String>();
					
					attr.put("to", "iWRTC.ES01.SWRTC.0");
					attr.put("name", "SOCKET");
					attr.put("ctype", "udp");
					attr.put("type", MessageType.REQUEST);
					attr.put("invoke", InvokeGenerator.getInvokeWithPrefix(this.currentState+"-"+System.currentTimeMillis()));
					attr.put("val", this.sipResponseInstance.toString());
					
					rawDataOut = new EquinoxRawData();
					
					rawDataOut.setRawDataAttributes(attr);
					rawDataOut.setRet(RetNumber.NORMAL);
					
					this.listOutput.add(rawDataOut);
					
					this.nextState  = States.W_ANSWER;
					
					break;

				default:
					break;
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawDataInput.getRet());
			
			switch (e) {
			case ERROR:
				
				
				break;

			case REJECT:
				
				break;
				
			
			case ABORT:
				
				
				break;
				
				
			case TIMEOUT:
				
				
				break;
				
				
			default:
				break;
			}
		
		}
		
		this.transactionTime = System.currentTimeMillis() - this.startTime;
		
		this.appInstance.setInvoke(rawDataInput.getInvoke());
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.appInstance.setSipResponseInstance(sipResponseInstance);
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setTimeout("10");
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		
		
		return this.nextState;
	}

}
