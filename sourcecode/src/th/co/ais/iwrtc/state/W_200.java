package th.co.ais.iwrtc.state;


import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.SIPResponse;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.utils.SIPParser;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxProperties;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_200 implements IAFState{

	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private String nextStat;
	private String currentState;
	private SIPResponse sipResponseInstance;
	private long startTime;
	private long transactionTime;
	
	
	@Override
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData> rawData) {
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.listOutput = new ArrayList<EquinoxRawData>();
		this.appInstance = this.ec02Instance.getAppInstance();
		this.currentState = af.getEquinoxProperties().getState();
		HashMap<String, String> attr = null ;
		
		EquinoxRawData rawDataInput = rawData.get(0);
		
		if(rawDataInput.getRet().equals(RetNumber.NORMAL)){
		
		
		try {
			
			this.sipResponseInstance = (SIPResponse) SIPParser.getMessage(rawDataInput.getRawDataCDATAAttributes("val"));
			
			if(!sipResponseInstance.getResponseCode().equals("200")){
				
			
				
				
				
			}
			
			else{
				
				attr = new HashMap<String, String>();
				attr.put("name", "SOCKET");
				attr.put("ctype", "udp");
				attr.put("invoke", this.appInstance.getInvoke());
				attr.put("type",MessageType.RESPONSE);
				attr.put("to", this.appInstance.getOrig());
				attr.put("val", this.sipResponseInstance.toString());
				
				EquinoxRawData rawDataOut = new EquinoxRawData();
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.TERMINATE);
				
				this.listOutput.add(rawDataOut);
				
				this.nextStat = States.IDLE;
			}
			
			
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}//if check ret 0
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawDataInput.getRet());
			
			switch(e){
			
			case ERROR:
				
				
				
				break;
				
				
			case REJECT:
				
				
				
				break;
				
				
				
			case ABORT:
				
				
				
				break;
				
				
			case TIMEOUT:
				
				
				
				break;
				
			default:
				
				
				
				break;
	
			}//switch check ret 1,2,3,4
				
		}//else check ret 
		
		this.transactionTime = System.currentTimeMillis() - this.startTime;
		
		this.appInstance.setInvoke(rawDataInput.getInvoke());
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.appInstance.setSipResponseInstance(sipResponseInstance);
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setEqxProp(af.getEquinoxProperties());
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setTimeout("10");
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		
		return this.nextStat;
	}

}
