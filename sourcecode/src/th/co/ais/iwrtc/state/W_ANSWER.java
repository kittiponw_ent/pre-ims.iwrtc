package th.co.ais.iwrtc.state;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.iwrtc.enums.EquinoxEvent;
import th.co.ais.iwrtc.enums.SIPResponseCode;
import th.co.ais.iwrtc.instances.APPInstance;
import th.co.ais.iwrtc.instances.EC02Instance;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPResponse;
import th.co.ais.iwrtc.interfaces.MessageType;
import th.co.ais.iwrtc.interfaces.RetNumber;
import th.co.ais.iwrtc.interfaces.States;
import th.co.ais.iwrtc.utils.InvokeGenerator;
import th.co.ais.iwrtc.utils.SIPParser;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_ANSWER implements IAFState {

	private String nextState;
	private APPInstance appInstance;
	private EC02Instance ec02Instance;
	private ArrayList<EquinoxRawData> listOutput;
	private SIPMessage sipMsginstance;
	private SIPResponse sipResponseInstance;
	private long startTime;
	private long transactionTime;
	private String currentState;
	
	@Override
	public String doAction(AbstractAF af, Object instance,ArrayList<EquinoxRawData>rawData) {
		this.currentState = af.getEquinoxProperties().getState();
		this.startTime = System.currentTimeMillis();
		this.ec02Instance = (EC02Instance) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		this.listOutput = new ArrayList<EquinoxRawData>();
		HashMap<String, String> attr = null;
		EquinoxRawData  rawDataOut =null;
		
		EquinoxRawData rawDataInput = rawData.get(0);
		
		if(RetNumber.NORMAL.equals(rawDataInput.getRet())){
		
		
		try {
			
			this.sipMsginstance = SIPParser.getMessage(rawDataInput.getRawDataCDATAAttributes("val"));
			
			this.sipResponseInstance = (SIPResponse) sipMsginstance;
			
			SIPResponseCode sipResponseCode = SIPResponseCode.getSipResponseCodeFrom(this.sipResponseInstance.getResponseCode());
			
			switch (sipResponseCode) {
			case OK:
				
				attr = new HashMap<String, String>();
				
				attr.put("name", "SOCKET");
				attr.put("to", "iWRTC.ES01.SWRTC.0");
				attr.put("invoke", InvokeGenerator.getInvokeWithPrefix(this.currentState+"-"+System.currentTimeMillis()));
				attr.put("ctype", "udp");
				attr.put("type", MessageType.REQUEST);
				attr.put("val", this.sipResponseInstance.toString());
				
				rawDataOut = new EquinoxRawData();
				
				rawDataOut.setRawDataAttributes(attr);
				rawDataOut.setRet(RetNumber.NORMAL);
				
				this.listOutput.add(rawDataOut);
				
				this.nextState = States.INITIIATE_COMMUNICATION;
				
				break;

			default:
				break;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}
		
		else{
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom(rawDataInput.getRet());
			
			switch (e) {
			case ERROR:
				
				
				
				break;

			case REJECT:
				
				
				break;
				
			
			case ABORT:
				
				
				
				break;
				
				
			case TIMEOUT:
				
				
				break;
				
			default:
				break;
			}
				
		}
		
		this.transactionTime = System.currentTimeMillis() - this.startTime;
		
		
		this.appInstance.setInvoke(rawDataInput.getInvoke());
		this.appInstance.setOrig(rawDataInput.getOrig());
		this.appInstance.setSipResponseInstance(sipResponseInstance);
		this.ec02Instance.setAbstractAF(af);
		this.ec02Instance.setAppInstance(this.appInstance);
		this.ec02Instance.setEquinoxRawDataList(this.listOutput);
		this.ec02Instance.setEqxProp(af.getEquinoxProperties());
		this.ec02Instance.setSession(af.getEquinoxProperties().getSession());
		this.ec02Instance.setTimeout("10");
		
		
		
		return this.nextState;
	}

}
