package th.co.ais.iwrtc.exceptions;

public class ValidatateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String statusCode;
	public String reasonPhase;
	
	
	
	public ValidatateException(String errorMsg){
		super(errorMsg);
	}
	
	public ValidatateException(String statusCode,String reasonPhase){
		
		this.statusCode = statusCode;
		this.reasonPhase = reasonPhase;
		
	}
	
	
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}



	public String getReasonPhase() {
		return reasonPhase;
	}



	public void setReasonPhase(String reasonPhase) {
		this.reasonPhase = reasonPhase;
	}

	
}
