package th.co.ais.iwrtc.exceptions;

public class SIPParserException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String errorCode;
	String reasonPhase;
	
	
	public SIPParserException(String errorDesc){
		super(errorDesc);
	}	
	
	public SIPParserException(String errorDesc,String reasonPhase){
		this.errorCode = errorDesc;
		this.reasonPhase = reasonPhase;
		
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getReasonPhase() {
		return reasonPhase;
	}

	public void setReasonPhase(String reasonPhase) {
		this.reasonPhase = reasonPhase;
	}	
	
	
}
