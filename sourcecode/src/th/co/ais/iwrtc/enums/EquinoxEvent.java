package th.co.ais.iwrtc.enums;

import java.util.HashMap;

public enum EquinoxEvent {

	NORMAL ("0","SUCCESS"),
	ERROR ("1","ERROR"),
	REJECT ("2","REJECT"),
	ABORT ("3","ABORT"),
	TIMEOUT ("4","TIMEOUT")
	;
	
	public String ret;
	public String desc;
	
	private EquinoxEvent(String ret,String desc){
		this.ret = ret;
		this.desc = desc;
		
	}

	public String getRet() {
		return ret;
	}

	public void setRet(String ret) {
		this.ret = ret;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	private static final HashMap<String, EquinoxEvent> lookup = new HashMap<String, EquinoxEvent>();
	static{
		for(EquinoxEvent e : EquinoxEvent.values()){
			lookup.put(e.getRet() , e);
		}	
	}
	
	
	public static EquinoxEvent getEquinoxEventFrom(String ret){
		return lookup.get(ret);
	}
	
}
