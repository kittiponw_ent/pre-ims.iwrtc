package th.co.ais.iwrtc.enums;

import java.util.HashMap;

public enum SIPMethodRequest {

	INVITE("INVITE"),
	OPTION("OPTION"),
	BYE("BYE"),
	CANCEL("CANCEL"),
	ACK("ACK"),
	REGISTER("REGISTER"),
	UPDATE("UPDATE"),
	PRACK("PRACK")
;

	private String method;
	
	private SIPMethodRequest(String method){
		this.method = method;
		
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	private static final HashMap<String, SIPMethodRequest> lookup = new HashMap<String, SIPMethodRequest>();
	static{
		for(SIPMethodRequest sip : SIPMethodRequest.values()){
			lookup.put(sip.getMethod(), sip);
			
		}	
	}
	
	public static SIPMethodRequest getSIPRequestMethodFrom(String method){
		return lookup.get(method);
		
	}
}
