package th.co.ais.iwrtc.enums;

import java.util.HashMap;

import ec02.af.utils.AlarmCategory;
import ec02.af.utils.AlarmSeverity;
import ec02.af.utils.AlarmType;

public enum Alarm {
	INCOMPLETE_REGISTER_PARAMETER("INCOMPLETE_REGISTER_PARAMETER",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	INCOMPLETE_INVITE_PARAMETER ("INCOMPLETE_INVITE_PARAMETER",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	UAA_RESPONSE_FAILED("UAA_RESPONSE_FAILED",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	UAA_INCOMPLETE_PARAMETER("UAA_INCOMPLETE_PARAMETER",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	SWRTC_RESPONSE_FAILED("sWRTC_RESPONSE_FAILED",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	SWRTC_SELECTED_FAILED ("sWRTC_SELECTED_FAILED",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	SWRTC_REGISTER_FAILED ("sWRTC_REGISTER_FAILED",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	LIA_RESPONSE_FAILED("LIA_RESPONSE_FAILED",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	RECEIVE_T_TRYING_FROM_SWRTC ("RECEIVE_T_TRYING_FROM_sWRTC",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	RECEIVE_T_200_OK_FROM_SWRTC ("RECEIVE_T_200_OK_FROM_sWRTC",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	UAR_TIMEOUT ("UAR_TIMEOUT",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	UAR_REJECT ("UAR_REJECT",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	UAR_ERROR ("UAR_ERROR",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	UAR_ABORT ("UAR_ABORT",AlarmSeverity.WARNING,AlarmCategory.APPLICATION,AlarmType.Normal),
	;
	
	
	private String alarmName;
	private AlarmSeverity severity;
	private AlarmCategory category;
	private AlarmType type;
	
	private Alarm(String alarmName,AlarmSeverity serverity,AlarmCategory category,AlarmType type){
		
		this.alarmName = alarmName;
		this.severity = serverity;
		this.category = category;
		this.type = type;
		
	}

	public String getAlarmName() {
		return alarmName;
	}

	public void setAlarmName(String alarmName) {
		this.alarmName = alarmName;
	}

	public AlarmSeverity getSeverity() {
		return severity;
	}

	public void setSeverity(AlarmSeverity severity) {
		this.severity = severity;
	}

	public AlarmCategory getCategory() {
		return category;
	}

	public void setCategory(AlarmCategory category) {
		this.category = category;
	}

	public AlarmType getType() {
		return type;
	}

	public void setType(AlarmType type) {
		this.type = type;
	}
	
	private static HashMap<String, Alarm> lookup = new HashMap<String, Alarm>();
	static{
		for(Alarm alarm : Alarm.values()){
			lookup.put(alarm.getAlarmName(), alarm);
		}
		
	}
	
	private static Alarm getAlarmNameFrom (String alarmName){
		
		return lookup.get(alarmName);
		
	}

}
