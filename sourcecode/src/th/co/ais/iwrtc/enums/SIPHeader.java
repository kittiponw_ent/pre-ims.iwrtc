package th.co.ais.iwrtc.enums;

import java.util.HashMap;

public enum SIPHeader {
	VIA("Via"),
	FROM("From"),
	TO("To"),
	CALL_ID("Call-ID"),
	CSEQ("CSeq"),
	CONTACT("Contact"),
	MAX_FORWARS("Max-Forwards"),
	CONTENT_LENGTH("Content-Length"),
	ACCEPT("Accept"),
	USER_AGENT("User-Agent"),
	AUTHORIZATION ("Authorization"),
	ALLOW ("Allow"),
	SUPPORTED("Supported"),
	SUBJECT("Subject"),
	WWW_AUTHENTICATE("WWW-Authenticate"),
	SERVER("Server"),
	CONTENT_TYPE("Content-Type")
	;
	
	private String header;
	
	private SIPHeader(String header){
		this.header = header;
		
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
	
	
	private static HashMap<String, SIPHeader> lookup = new HashMap<String, SIPHeader>();
	static{
		for(SIPHeader sipHeader : SIPHeader.values())
		{
			lookup.put(sipHeader.getHeader(), sipHeader);			
		}
		
	}
	
	public static SIPHeader getHeaderFrom(String header){
		
		return lookup.get(header);
	}
	
	
}
