package th.co.ais.iwrtc.enums;

import java.util.HashMap;

public enum Statistic {

	INCOMPLETE_REGISTER_PARAMETER("INCOMPLETE_REGISTER_PARAMETER"),
	INCOMPLETE_INVITE_PARAMETER ("INCOMPLETE_INVITE_PARAMETER"),
	SEND_TRYING_TO_O_SWRTC_SUCCESS("SEND_TRYING_TO_O_SWRTC_SUCCESS"),
	UAA_RESPONSE_FAILED("UAA_RESPONSE_FAILED"),
	UAA_INCOMPLETE_PARAMETER("UAA_INCOMPLETE_PARAMETER"),
	REQUEST_UAR_TO_MAAA ("REQUEST_UAR_TO_mAAA"), 
	SWRTC_RESPONSE_FAILED("sWRTC_RESPONSE_FAILED"),
	SWRTC_SELECTED_FAILED ("sWRTC_SELECTED_FAILED"),
	SWRTC_REGISTER_FAILED ("sWRTC_REGISTER_FAILED"),
	LIA_RESPONSE_FAILED("LIA_RESPONSE_FAILED"),
	RECEIVE_T_TRYING_FROM_sWRTC ("RECEIVE_T_TRYING_FROM_sWRTC"),
	RECEIVE_T_200_OK_FROM_SWRTC ("RECEIVE_T_200_OK_FROM_sWRTC"),
	RECEIVE_T_183_FROM_SWRTC("RECEIVE_T_183_FROM_sWRTC"),
	RECEIVE_T_RINGING_FROM_SWRTC("RECEIVE_T_RINGING_FROM_sWRTC"),
	RECEIVE_O_ACK_FROM_SWRTC("RECEIVE_O_ACK_FROM_sWRTC"),
	UAR_TIMEOUT ("UAR_TIMEOUT"),
	UAR_REJECT ("UAR_REJECT"),
	UAR_ERROR ("UAR_ERROR"),
	UAR_ABORT ("UAR_ABORT"),
	;
	
	private String statName;
	
	private Statistic(String statName){
		this.statName = statName;
	}

	
	public String getStatName() {
		return statName;
	}

	public void setStatName(String statName) {
		this.statName = statName;
	}
	
	private static HashMap<String, Statistic> lookup = new HashMap<String, Statistic>();
	static{
		for(Statistic stat : Statistic.values()){
			lookup.put(stat.getStatName(), stat);
		}
		
	}
	
	private static Statistic getStatisticNameFrom (String statName){
		
		return lookup.get(statName);	
	}
	
}
