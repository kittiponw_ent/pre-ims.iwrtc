package th.co.ais.iwrtc.enums;

import java.util.HashMap;

public enum SIPResponseCode {

	TRYING("100","Trying"),
	SESSION_IN_PROGRESS("183","Session in Progress"),
	OK("200","OK"),
	RINGING("180","Ringing"),
	UNAUTHORIZED("401","Unauthorized"),
	SERVER_INTERNAL_ERROR("500","Server Internal Error"),
	SERVER_TIME_OUT("504","Server Time-out"),
	BAD_REQUEST("400","Bad Request")
	
	;
	
	private String code;
	private String reasonCode;
	
	private SIPResponseCode(String code , String reasonCode){
		this.code = code;
		this.reasonCode = reasonCode;
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}


	private static final HashMap<String, SIPResponseCode> lookup = new HashMap<String, SIPResponseCode>();
	static{
		for(SIPResponseCode sipRes : SIPResponseCode.values()){
			lookup.put(sipRes.getCode(), sipRes);
		}
	}	
		
	public static SIPResponseCode getSipResponseCodeFrom(String code){
		
		return lookup.get(code);
		
	}
			
	}
	
