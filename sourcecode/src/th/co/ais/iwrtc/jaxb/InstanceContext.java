package th.co.ais.iwrtc.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;


import th.co.ais.iwrtc.instances.LocationInfoAnswer;
import th.co.ais.iwrtc.instances.LocationInfoRequest;
import th.co.ais.iwrtc.instances.UserAuthorizeAnswer;
import th.co.ais.iwrtc.instances.UserAuthorizeRequest;


public class InstanceContext {

	private static JAXBContext userAuthorizeRequest;
	private static JAXBContext userAuthorizeAnswer;
	private static JAXBContext locationInfoRequest;
	private static JAXBContext locationInfoAnswer;
	
	/*
	 *create context  UserAuthorizeRequest
	 */
	
	public static synchronized void initUserAuthorizeRequestContext() throws JAXBException {
		if ( userAuthorizeRequest == null ){
			userAuthorizeRequest = JAXBContext.newInstance( UserAuthorizeRequest.class );
		}
	}
	
	/*
	 * get context UserAuthorizeRequest
	 */
	public static JAXBContext getUserAuthorizeRequestContext() {
		return userAuthorizeRequest;
	}
	
	
	/*
	 * create context UserAuthorizeAnswer
	 */
	
	public static synchronized void initUserAuthorizeAnswerContext() throws JAXBException{
		
		if(userAuthorizeAnswer == null){
			userAuthorizeAnswer = JAXBContext.newInstance(UserAuthorizeAnswer.class);
		}
		
	}
	
	/*
	 * get context UserAuthorizeAnswer
	 */
	public static JAXBContext getUserAuthorizeAnswerContext(){
		return userAuthorizeAnswer;
	}
	
	/*
	 * create context LocationInfoRequest
	 */
	
	public static synchronized void initLocationInfoRequestContext() throws JAXBException {
		if(locationInfoRequest == null){
			locationInfoRequest = JAXBContext.newInstance(LocationInfoRequest.class);
		}
	}
	
	/*
	 * get context LocationInfoRequest
	 */
	
	public static JAXBContext getLocationInfoRequestContext(){
		return locationInfoRequest;
	}
	
	/*
	 * create context LocationInfoAnswer
	 */
	
	
	public static synchronized void initLocationInfoAnswerContext() throws JAXBException{
		if(locationInfoAnswer == null){
			
			locationInfoAnswer = JAXBContext.newInstance(LocationInfoAnswer.class);
		}
		
	}
	
	/*
	 * get context LocationInfoAnswer
	 */
	
	public static JAXBContext getLocationInfoAnswerContext(){
		return locationInfoAnswer;
	}
	

}
