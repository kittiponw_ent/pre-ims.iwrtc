package th.co.ais.iwrtc.jaxb;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

public class JAXBHandle {

	// IMPORT MESSAGE TO INSTANCE [ UNMARSHALLING ]
		public static Object createInstance ( JAXBContext context, String message, Class<?> instanceClass ) {

			Object instance = null;
			
			Unmarshaller unmarshaller;
			try {
				unmarshaller = context.createUnmarshaller();
				
				instance = Class.forName( instanceClass.getCanonicalName() ).newInstance();
				instance = unmarshaller.unmarshal( new StringReader(message) );
				
			} catch( Exception e ) {
				e.printStackTrace();
				return null;
			}
			
			return instance;
		}
		
		// EXPORT INSTANCE TO MESSAGE [ MARSHALLING ]
		public static String composeMessage ( JAXBContext context, Object instance ) {
			
			StringWriter writer = new StringWriter();
			
			Marshaller marshaller;
			try {
				marshaller = context.createMarshaller();
				marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
				marshaller.setProperty( Marshaller.JAXB_FRAGMENT, true );
				
				marshaller.marshal( instance, writer );
				
			} catch( JAXBException e ) { 
				e.printStackTrace();
			}
			
			return writer.toString();
		}
		
		
		public static String composeMessageWithoutRoot( JAXBContext context, Object instance ){
			
			Marshaller marshaller;
			
			StringWriter writer = new StringWriter();
			
			try {
				marshaller = context.createMarshaller();
				marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
				marshaller.setProperty( Marshaller.JAXB_FRAGMENT, true );
				
				marshaller.marshal(new JAXBElement (new QName("", ""), context.getClass(), instance ), writer);
				
			} catch (JAXBException e) {
				e.printStackTrace();
			}
			
			String reWriter = writer.toString().replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
			return reWriter;
		}
		
	
}
