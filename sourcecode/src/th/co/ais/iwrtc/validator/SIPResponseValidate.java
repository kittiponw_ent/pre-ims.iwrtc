


package th.co.ais.iwrtc.validator;

import th.co.ais.iwrtc.exceptions.ValidatateException;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.interfaces.SIPMsgHeader;

public class SIPResponseValidate {

	public static void verifyMessage(SIPMessage sipMessage ) throws ValidatateException{
		
		/*
		 * check field header Via
		 */
		if(sipMessage.getHeader().get("Via").equals(null)){
			throw new ValidatateException("error in parameter : "+SIPMsgHeader.VIA);
		}
		
		else {
			
			
		}
		
		/*
		 * check field header Content-Length
		 */
		
		if(sipMessage.getHeader().get("Content-Length").equals(null)){
			throw new ValidatateException("error in parameter : "+SIPMsgHeader.CONTENT_LENGTH);	
		}
		else{
			
			
		}
		
		/*
		 * check field header From
		 */
		
		
		if(sipMessage.getHeader().get("From").equals(null)){
			throw new ValidatateException("error in parameter : "+SIPMsgHeader.FROM);
		}
		else{
			
		}
		
	
		
		/*
		 * check field header To
		 */
		
		if(sipMessage.getHeader().get("To").equals(null)){
			throw new ValidatateException("error in parameter : "+SIPMsgHeader.TO);
		}
		
		else{
			
		}
		
		
		/*
		 *check field header CSeq 
		 */
		
		if(sipMessage.getHeader().get("CSeq").equals(null)){
			throw new ValidatateException("error in parameter : "+SIPMsgHeader.CSEQ);
		}
		else{
			
		}
		
		/*
		 * check field header Call-ID
		 */
		
		if(sipMessage.getHeader().get("Call-ID").equals(null)){
			throw new ValidatateException("error in parameter : "+SIPMsgHeader.CALL_ID);
		}
		
		else{
			
		}
		
		if(sipMessage.getHeader().get("Content-Type").equals(null)){
			throw new ValidatateException("error in parameter : "+SIPMsgHeader.CALL_ID);
		}
		
		else{
			
		}
		
		
	}
	
	
}
