package th.co.ais.iwrtc.validator;

import th.co.ais.iwrtc.enums.SIPResponseCode;
import th.co.ais.iwrtc.exceptions.ValidatateException;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.interfaces.SIPMsgHeader;

public class SIPRequestValidate {

	public static void verifyMessage(SIPMessage sipMessage) throws  ValidatateException{
		
		/*
		 * check field header Via
		 */
		if(sipMessage.getHeader().get("Via").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		
		else {
			
			
			
			
		}
		
		/*
		 * check field header Content-Length
		 */
		
		if(sipMessage.getHeader().get("Content-Length").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		else{
			
			
		}
		
		/*
		 * check field header From
		 */
		
		
		if(sipMessage.getHeader().get("From").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		else{
			
		}
		
		/*
		 * check field header Accept
		 */
		
		if(sipMessage.getHeader().get("Accept").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		
		/*
		 * check field header User-Agent
		 */
		if(sipMessage.getHeader().get("User-Agent").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
			
		}
		
		else{
			
			
			
			
			
		}
		
		/*
		 * check field header To
		 */
		
		if(sipMessage.getHeader().get("To").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		
		else{
			
			
			
			
			
		}
		
		/*
		 * check field header contact
		 */
		
		if(sipMessage.getHeader().get("Contact").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		
		else{
			
		}
		
		/*
		 *check field header CSeq 
		 */
		
		if(sipMessage.getHeader().get("CSeq").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		else{
			
		}
		
		/*
		 * check field header Call-ID
		 */
		
		if(sipMessage.getHeader().get("Call-ID").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		
		else{
			
		}
		
		/*
		 * check field header Authorization
		 */
		
		
		if(sipMessage.getHeader().get("Authorization").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		
		else{
			
			
		}
		
		/*
		 * check field header Max-Forwards
		 */
		
		if(sipMessage.getHeader().get("Authorization").equals(null)){
			throw new ValidatateException(
					SIPResponseCode.BAD_REQUEST.getCode(),
					SIPResponseCode.BAD_REQUEST.getReasonCode());
		}
		
		else{
			
			
			
		}
		
		
		
	}//method verifyMessage
	
}//class SIPRequestValidate
