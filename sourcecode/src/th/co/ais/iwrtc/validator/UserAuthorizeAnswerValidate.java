package th.co.ais.iwrtc.validator;

import th.co.ais.iwrtc.enums.SIPResponseCode;
import th.co.ais.iwrtc.exceptions.ValidatateException;
import th.co.ais.iwrtc.instances.UserAuthorizeAnswer;

public class UserAuthorizeAnswerValidate {

	public static void verifyMessage(UserAuthorizeAnswer uaa) throws ValidatateException{
		
		if(uaa.getSessionId().equals(null)){
			throw new ValidatateException(
					SIPResponseCode.SERVER_INTERNAL_ERROR.getCode(),
					SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode());
		}
		else{
			
			
		}
		
		if(uaa.getVender_Spec_Application_ID().equals(null)){
			
			throw new ValidatateException(
					SIPResponseCode.SERVER_INTERNAL_ERROR.getCode(),
					SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode());
			
		}
		
		else{
			
			
		}
		
		if(uaa.getAuth_Session_State().equals(null)){
			throw new ValidatateException(
					SIPResponseCode.SERVER_INTERNAL_ERROR.getCode(),
					SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode());
			
		}
		
		else{
			
			
		}
		
		if(uaa.getOrigin_Host().equals(null)){
			throw new ValidatateException(
					SIPResponseCode.SERVER_INTERNAL_ERROR.getCode(),
					SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode());
		}
		
		else{
			
			
		}
		
		if(uaa.getOrigin_Realms().equals(null)){
			throw new ValidatateException(
					SIPResponseCode.SERVER_INTERNAL_ERROR.getCode(),
					SIPResponseCode.SERVER_INTERNAL_ERROR.getReasonCode());
		}
		
		else{
		
			
		}
		
		
		
	}
	
}
