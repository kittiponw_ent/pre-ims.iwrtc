package th.co.ais.iwrtc.interfaces;

public interface RetNumber {
	
	public static String NORMAL="0"; 
	public static String ERROR="1";
	public static String REJECT="2";
	public static String ABORT="3";
	public static String TIMEOUT="4";
	public static String TERMINATE="10";
	
}
