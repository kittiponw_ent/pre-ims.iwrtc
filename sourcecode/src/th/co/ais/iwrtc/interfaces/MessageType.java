package th.co.ais.iwrtc.interfaces;

public interface MessageType {

	public static String REQUEST="request";
	public static String RESPONSE="response";
	
}
