package th.co.ais.iwrtc.interfaces;

public interface SIPMsgHeader {

	String VIA ="Via";
	String FROM = "From";
	String TO = "To";
	String CALL_ID="Call-ID";
	String CSEQ = "CSeq";
	String CONTACT = "Contact";
	String MAX_FORWARS = "Max-Forwards";
	String CONTENT_LENGTH= "Content-Length";
	String ACCEPT = "Accept";
	String USER_AGENT = "User-Agent";
	String AUTHORIZATION = "Authorization";
	String ALLOW = "Allow";
	String SUPPORTED="Supported";
	String WWW_AUTHENTICATE = "WWW-Authenticate";
	String SERVER ="Server";
	String SUBJECT ="Subject";
	String CONTENT_TYPE="Content-Type";
}
