package th.co.ais.iwrtc.interfaces;

public interface States {

	String IDLE ="IDLE";
	String W_200="W_200";
	String W_2ND_REGISTER="W_2ND_REGISTER";
	String W_401="W_401";
	String W_UAA="W_UAA";
	String W_LIA = "W_LIA";
	String INITIATE_INVITE = "INITIATE_INVITE";
	String W_RINGING = "W_RINGING";
	String W_ANSWER = "W_ANSWER";
	String INITIIATE_COMMUNICATION = "INITIIATE_COMMUNICATION";
}
