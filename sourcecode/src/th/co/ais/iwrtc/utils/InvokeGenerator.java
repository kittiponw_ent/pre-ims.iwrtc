package th.co.ais.iwrtc.utils;

import java.util.UUID;

public class InvokeGenerator {

	public static String getRandomInvoke(){
		
		return UUID.randomUUID().toString();
		
	}
	
	
	public static String getInvokeWithPrefix(String invoke){
		
		return invoke+"-"+UUID.randomUUID().toString().toUpperCase().split("-")[0];
		
	}
	

}
