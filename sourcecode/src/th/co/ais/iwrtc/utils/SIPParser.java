package th.co.ais.iwrtc.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;


import java.util.HashMap;

import th.co.ais.iwrtc.enums.SIPMethodRequest;
import th.co.ais.iwrtc.exceptions.SIPParserException;
import th.co.ais.iwrtc.instances.SIPMessage;
import th.co.ais.iwrtc.instances.SIPRequest;
import th.co.ais.iwrtc.instances.SIPResponse;
import th.co.ais.iwrtc.interfaces.SIPMsgHeader;



public class SIPParser {
	
public static final String SIPSEPARATOR = ":";
	
	
	public static SIPMessage getMessage(String sipMessage) throws SIPParserException{
		
	BufferedReader br = new BufferedReader(new StringReader(sipMessage));
	SIPMessage sipMsgInstance =null;
		
		try{
		String startLine = br.readLine();
		int secSpacePosition = startLine.indexOf(" ")+1;//position space
		
		/*
		 * check start line is request or response 
		 */
		
		if(SIPMethodRequest.getSIPRequestMethodFrom(startLine.substring(0,startLine.indexOf(" ")))!=null ){
			String method = startLine.substring(0,startLine.indexOf(" "));
			String uri = startLine.substring(startLine.indexOf(" ")+1,startLine.indexOf(" ", secSpacePosition));
			String version = startLine.substring(startLine.indexOf(" ", secSpacePosition)+1);
			sipMsgInstance = new SIPRequest(method, uri, version);
			
		}//check request
		
		else if(startLine.substring(0,startLine.indexOf(" ")).equals("SIP/2.0")){
			String version = startLine.substring(0,startLine.indexOf(" "));
			String responseCode = startLine.substring(startLine.indexOf(" ")+1,startLine.indexOf(" ", secSpacePosition));
			String reasonCode = startLine.substring(startLine.indexOf(" ", secSpacePosition)+1);
			sipMsgInstance = new SIPResponse(version,responseCode,reasonCode);
		}//check response
		
		else {
			throw new SIPParserException("message error : "+startLine);
		}//else
		
		
		/*
		 * get message Header  
		 */
		
		String line,content;
		StringBuilder msgContent = new StringBuilder();
		ArrayList<String> valueList;
		HashMap<String, ArrayList<String>> head = new HashMap<String, ArrayList<String>>();
		
		while(true){
			line = br.readLine();
			if(line == null || "\n".equals(line) || line.length() == 0){
				break;
			}//check line null
			
			if(line.indexOf(SIPSEPARATOR) ==-1){
				throw new SIPParserException("Message Error at : "+ line);
			}//check missing separator :
				
			
			String headerField = line.substring(0,line.indexOf(SIPSEPARATOR));	
			String valuefield = line.substring(line.indexOf(SIPSEPARATOR)+1).trim();
			
			if(SIPMsgHeader.VIA.equals(headerField)){
				valueList = new ArrayList<String>();
				valueList.add(" "+valuefield);
				head.put(headerField, valueList);
			}
			
			else{
			valueList = new ArrayList<String>();
			valueList.add(" "+valuefield);
			head.put(headerField, valueList );
			}
			
		}//while
		
		/*
		 * get message body
		 */
		msgContent.append("\n");
		while((content=br.readLine())!=null){
			msgContent.append(content+"\r\n");	
		}
		
		sipMsgInstance.setHeader(head);
		sipMsgInstance.setContent(msgContent);
				
		}//try
		catch(IOException io){
		return null;	
			
		}//close catch
	
	return sipMsgInstance;
	}//class getMessage

}//class SIPHandler
