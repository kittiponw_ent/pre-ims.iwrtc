package th.co.ais.iwrtc.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import th.co.ais.iwrtc.instances.APPInstance;

public class InstanceHandler {

	public static String encode(APPInstance appInstance){//encode instance
		Gson gson = new Gson();
		byte bytes[] = null;
		String encode="";
		try {
			bytes = Zip.compressBytes(gson.toJson(appInstance).getBytes());
			encode = Base64.encode(bytes);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return encode;
	}
	
	public static APPInstance decode(String instanceEncode){//decode instance
		Gson gson = new  Gson();
		byte decode[] = null;
		try {
			decode = Base64.decode(instanceEncode);
			decode = Zip.extractBytes(decode);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		APPInstance appInstance = gson.fromJson(new String(decode),APPInstance.class);
		
		return appInstance;
	}
}
