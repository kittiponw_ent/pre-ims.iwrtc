package th.co.ais.iwrtc.utils;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringEscapeUtils;

import com.google.gson.annotations.SerializedName;

public class APPDetail {

	@SerializedName("ApplicationName.Detail")
	private TransactionDetail transactionDetail;
	
	@SerializedName("CurrentState")
	private String currentState;
	
	@SerializedName("NextState")
	private String nextState;
	
	@SerializedName("ProcessingTime")
	private long processingTime;

	public APPDetail(){
		this.setTransactionDetail(new TransactionDetail());
	}
	
	
	public TransactionDetail getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(TransactionDetail transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public String getNextState() {
		return nextState;
	}

	public void setNextState(String nextState) {
		this.nextState = nextState;
	}

	public long getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(long processingTime) {
		this.processingTime = processingTime;
	}
	
	
	/*
	 * inner class TransactionDetail
	 * 
	 */
	
	public class TransactionDetail{
		
		@SerializedName("Session")
		private String session;
		
		@SerializedName("InitialInvoke")
		private String initialInvoke;
		
		@SerializedName("Scenario")
		private String scenario;
		
		@SerializedName("MSISDN")
		private String msisdn;
		
		@SerializedName("InputDetail")
		private ArrayList<InputDetail> inputDetail;
		
		@SerializedName("InputTimeStamp")
		private String inputTimeStamp;
		
		@SerializedName("OutputDetail")
		private ArrayList<OutputDetail> outputDetail;
		
		@SerializedName("OutputTimeStamp")
		private String outputTimeStamp;
		
		private transient String timeStampFormat = "yyyymmdd hh:mm:ss.SSS";

		public String getSession() {
			return session;
		}

		public void setSession(String session) {
			this.session = session;
		}

		public String getInitialInvoke() {
			return initialInvoke;
		}

		public void setInitialInvoke(String initialInvoke) {
			this.initialInvoke = initialInvoke;
		}

		public String getScenario() {
			return scenario;
		}

		public void setScenario(String scenario) {
			this.scenario = scenario;
		}

		public String getMsisdn() {
			return msisdn;
		}

		public void setMsisdn(String msisdn) {
			this.msisdn = msisdn;
		}

		public ArrayList<InputDetail> getInputDetail() {
			return inputDetail;
		}

		public void setInputDetail(ArrayList<InputDetail> inputDetail) {
			this.inputDetail = inputDetail;
		}
		
		public void addInputDetail(InputDetail inputDetail){
			if(this.inputDetail == null){
			this.inputDetail = new ArrayList<APPDetail.InputDetail>();	
				
			}
			this.inputDetail.add(inputDetail);
		}
		
	
		public ArrayList<OutputDetail> getOutputDetail() {
			return outputDetail;
		}

		public void setOutputDetail(ArrayList<OutputDetail> outputDetail) {
			this.outputDetail = outputDetail;
		}

		public void addOutputDetail(OutputDetail outputDetail){
			
			if(this.outputDetail == null){
				this.outputDetail = new ArrayList<APPDetail.OutputDetail>();
			}
			
			this.outputDetail.add(outputDetail);
		
		}
		
		public String getInputTimeStamp() {
			return inputTimeStamp;
		}

		public void setInputTimeStamp(String inputTimeStamp) {
			this.inputTimeStamp = inputTimeStamp;
		}
		
		
		public String getOutputTimeStamp() {
			return outputTimeStamp;
		}

		public void setOutputTimeStamp(String outputTimeStamp) {
			this.outputTimeStamp = outputTimeStamp;
		}

		public String getTimeStampFormat() {
			return timeStampFormat;
		}

		public void setTimeStampFormat(String timeStampFormat) {
			this.timeStampFormat = timeStampFormat;
		}
				
	}//inner class TransactionDetail 
	
	/*
	 * 
	 * inner class IODetail
	 * 
	 */
	
	public class IODetail{
		
		@SerializedName("Invoke")
		private String invoke;
		
		@SerializedName("Event")
		private String event;
		
		@SerializedName("Type")
		private String type;
		
		@SerializedName("RawDataAttribute")
		private HashMap<String, String> rawDataAttibute;
		
		@SerializedName("RawDataMessage")
		private String rawDataMessage;
		
		@SerializedName("Data")
		private Object data;

		public String getInvoke() {
			return invoke;
		}

		public void setInvoke(String invoke) {
			this.invoke = invoke;
		}

		public String getEvent() {
			return event;
		}

		public void setEvent(String event) {
			this.event = event;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public HashMap<String, String> getRawDataAttibute() {
			return rawDataAttibute;
		}

		public void setRawDataAttibute(HashMap<String, String> rawDataAttibute) {
			this.rawDataAttibute = rawDataAttibute;
		}

		public String getRawDataMessage() {
			return rawDataMessage;
		}

		public void setRawDataMessage(String rawDataMessage) {
			
			if(rawDataMessage != null){
			
			this.rawDataMessage = StringEscapeUtils.unescapeHtml(rawDataMessage);
			}
		}

		public Object getData() {
			return data;
		}

		public void setData(Object data) {
			this.data = data;
		}
		
	}//inner class IODetail
	
	/*
	 * inner class inputDetail
	 * 
	 */
	
	public class InputDetail extends IODetail{
		
		@SerializedName("ResponseTime")
		private long responseTime;
		
		public InputDetail(String invoke,APPDetailEvent event,String type,HashMap<String, String>rawDataAttribute,String rawDataMessage,long responseTime){
			this.setInvoke(invoke);
			this.setEvent(event.toString());
			this.setType(type);
			this.setRawDataAttibute(rawDataAttribute);
			this.setRawDataMessage(rawDataMessage);
			this.setResponseTime(responseTime);
		}

		public long getResponseTime() {
			return responseTime;
		}

		public void setResponseTime(long responseTime) {
			this.responseTime = responseTime;
		}
		
		
	}//inner class InputDetail
	
	
	/*
	 * inner class OutputDetail
	 * 
	 */
	
	public class OutputDetail extends IODetail{
		
		public OutputDetail(String invoke,APPDetailEvent event,String type,HashMap<String, String> rawDataAttribute,String rawDataMessage){
			this.setInvoke(invoke);
			this.setEvent(event.toString());
			this.setType(type);
			this.setRawDataAttibute(rawDataAttribute);
			this.setRawDataMessage(rawDataMessage);
		}
		
	}//inner class OutputDetail
	
}//class APPDetail
