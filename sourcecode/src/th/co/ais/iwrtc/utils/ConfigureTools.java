package th.co.ais.iwrtc.utils;

import java.util.ArrayList;
import java.util.HashMap;



public class ConfigureTools {

private static HashMap<String, ArrayList<String>> warmConfig;
	
	public static void initConfigureTool( HashMap<String, ArrayList<String>> hmWarmConfig ) {
		warmConfig = hmWarmConfig;
	}
	
	public static String getConfigure( String configName ) {
		try {
			return warmConfig.get( configName ).get(0);
			
		} catch (Exception e) {
			return null;
		}
		
	}
	
	
}
