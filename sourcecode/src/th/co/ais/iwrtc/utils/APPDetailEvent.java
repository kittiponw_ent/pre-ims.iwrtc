package th.co.ais.iwrtc.utils;

public class APPDetailEvent {

	private String nodeName;
	private String scenario;
	
	private static final String delim = ".";
	
	public APPDetailEvent(String nodeName,String scenario){
		
		this.nodeName = nodeName;
		this.scenario = scenario;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public static String getDelim() {
		return delim;
	}
	
	public String toString(){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(this.getNodeName()+delim);
		sb.append(this.getScenario());
		
		return sb.toString();
	}
	
}
