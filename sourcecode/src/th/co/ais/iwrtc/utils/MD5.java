package th.co.ais.iwrtc.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {

	private static MessageDigest messageDigest;
	
	static {
		
		try {
			
			messageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	
	public static String encrypt(String encrypt){
		
		if(encrypt==null){
			throw new IllegalArgumentException("input to encrypt is null or zero byte");
			
		}
		
		messageDigest.update(encrypt.getBytes());
		
		byte[] hashByte = messageDigest.digest();
		
		StringBuilder sb = new StringBuilder();
		
		for(byte b : hashByte){
			
			sb.append(String.format( "%02x", b & 0xff ));
		}
		
		return sb.toString();
		
	}
	
}
